import {
  Directive,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { AbstractControl, FormGroupDirective } from '@angular/forms';
import { startWith } from 'rxjs/operators';
import * as _ from 'lodash';
import { Control } from '../model/control.model';

interface ErrorTemplateContext {
  $implicit: string;
}

interface MaxLength {
  requiredLength: number;
  actualLength: number;
}

interface TypedValidationErrors {
  maxValue: number;
}

function renderMessage<K extends keyof TypedValidationErrors>(
  field: string,
  key: K,
  error: TypedValidationErrors[K]
): string {
  switch (key) {
    case 'required':
      return `${field} is required`;
  }
}

export type ValidationErrorsOfInput = string | undefined | Control<any>;

@Directive({
  selector: '[appValidationErrors]',
})
export class ValidationErrorsDirective implements OnChanges, OnDestroy {
  @Input() appValidationErrorsOf: ValidationErrorsOfInput;
  @Input() appValidationErrorsBy = 'field';

  private statusSub = Subscription.EMPTY;

  constructor(
    private vcr: ViewContainerRef,
    private template: TemplateRef<ErrorTemplateContext>,
    private formGroup: FormGroupDirective
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('appValidationErrorsOf' in changes) {
      this.processControlNameChange(changes);
    }
  }

  ngOnDestroy(): void {
    this.statusSub.unsubscribe();
  }

  private processControlNameChange(changes: SimpleChanges) {
    this.statusSub.unsubscribe();

    const input = changes.appValidationErrorsOf
      .currentValue as ValidationErrorsOfInput;
    if (input !== undefined) {
      const controlName = _.isObjectLike(input)
        ? (input as Control<any>).key
        : (input as string);
      const control = this.formGroup.control.controls[controlName];
      this.statusSub = control.statusChanges
        .pipe(startWith(control.status))
        .subscribe(() => this.redrawErrors(control, input));
    }
  }

  private redrawErrors(control: AbstractControl, input: string | Control<any>) {
    this.vcr.clear();
    const errors = control.errors as TypedValidationErrors | null;

    if (errors == null) {
      return;
    }

    const controlLabel = _.isObjectLike(input)
      ? (input as Control<any>).label
      : (this.appValidationErrorsBy as string);
    for (const key in errors) {
      if (!errors.hasOwnProperty(key)) {
        continue;
      }
      this.vcr.createEmbeddedView(this.template, {
        $implicit: renderMessage(
          controlLabel,
          key as keyof TypedValidationErrors,
          errors[key]
        ),
      });
    }
  }
}
