import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidationErrorsDirective } from './validation-errors/validation-errors.directive';

@NgModule({
  declarations: [ValidationErrorsDirective],
  imports: [CommonModule],
  exports: [ValidationErrorsDirective],
})
export class ConfettiFormsModule {}
