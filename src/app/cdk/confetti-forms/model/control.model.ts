export interface Control<T> {
  readonly label: string;
  readonly key: T;
}

export type FormKeys<T> = Record<keyof T, Control<keyof T>>;
