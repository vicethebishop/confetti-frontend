import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../service/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

const CONTROLS = {
  email: 'email',
  password: 'password',
  firstName: 'firstName',
  lastName: 'lastName',
  bio: 'bio',
};

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  CONTROLS = CONTROLS;
  form: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly activatedRoute: ActivatedRoute
  ) {
    this._initForm();
  }

  ngOnInit(): void {}

  private _initForm() {
    this.form = this.fb.group({
      [CONTROLS.email]: ['', [Validators.required, Validators.email]],
      [CONTROLS.password]: ['', Validators.required],
      [CONTROLS.firstName]: ['', Validators.required],
      [CONTROLS.lastName]: ['', Validators.required],
      [CONTROLS.bio]: '',
    });
  }

  submit() {
    if (this.form.invalid) {
      Object.values(this.form.controls).forEach(control =>
        control.markAsTouched()
      );
      this.form.markAsTouched();
      return;
    }
    this.authService.register(this.form.value).subscribe(val =>
      this.router.navigate(['../login'], {
        relativeTo: this.activatedRoute,
      })
    );
  }

  goToLogin() {
    this.router.navigate(['../login'], {
      relativeTo: this.activatedRoute,
    });
  }
}
