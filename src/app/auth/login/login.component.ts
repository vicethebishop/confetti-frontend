import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginModel } from '../model/login.model';
import { AuthService } from '../service/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

const CONTROLS = {
  email: 'username',
  password: 'password',
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  readonly CONTROLS = CONTROLS;

  constructor(
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly activatedRoute: ActivatedRoute
  ) {
    this._initForm();
  }

  form: FormGroup;

  ngOnInit() {}

  private _initForm() {
    this.form = this.fb.group({
      [CONTROLS.email]: ['', [Validators.required, Validators.email]],
      [CONTROLS.password]: ['', [Validators.required]],
    });
  }

  submit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    const { username, password } = this.form.value;
    this.authService.login(username, password).subscribe(
      value => {
        this.router.navigate(['']);
      },
      error =>
        this.snackBar.open('Email or password are invalid', null, {
          duration: 3000,
        })
    );
  }

  redirectToRegistration() {
    this.router.navigate(['../register'], {
      relativeTo: this.activatedRoute,
    });
  }
}
