import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Credentials {
  token: {
    access_token: string;
  };
}

const KEY = 'credentials';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  private readonly tokenHelper = new JwtHelperService();
  private _credentials$ = new BehaviorSubject<Credentials>(null);

  constructor() {
    const savedCredentials =
      sessionStorage.getItem(KEY) || localStorage.getItem(KEY);
    if (savedCredentials) {
      this._credentials$.next(JSON.parse(savedCredentials));
    }
  }

  public get credentials() {
    return this._credentials$.value;
  }

  public set credentials(value: Credentials) {
    this._credentials$.next(value);

    if (value) {
      localStorage.setItem(KEY, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(KEY);
      localStorage.removeItem(KEY);
    }
  }

  public get authenticated() {
    return this._credentials$.pipe(map(value => !!value && !!value.token));
  }

  public get userId(): number {
    return this.tokenHelper.decodeToken(
      this._credentials$.value.token.access_token
    ).id;
  }

  public get username(): string {
    return this.tokenHelper.decodeToken(
      this._credentials$.value.token.access_token
    ).username;
  }
}
