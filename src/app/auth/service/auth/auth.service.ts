import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../token/token.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { RegistrationModel } from '../../model/registration.model';

const routes = {
  login: `auth/login`,
  register: 'auth/register',
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private readonly http: HttpClient,
    private readonly tokenService: TokenService,
    private readonly router: Router,
    private readonly httpClient: HttpClient
  ) {}

  public login(username: string, password: string): Observable<boolean> {
    return this.http
      .post<{ access_token: string }>(routes.login, {
        username: username,
        password,
      })
      .pipe(
        tap(
          value =>
            (this.tokenService.credentials = {
              token: value,
            })
        ),
        map(value => !!value)
      );
  }

  public logout() {
    this.tokenService.credentials = undefined;
    this.router.navigate(['/auth/login']);
  }

  public register(model: RegistrationModel) {
    return this.httpClient.post(routes.register, model);
  }
}
