import { Role } from '../../../commons/constants/role';

export interface UserModel {
  id?: number;
  firstName?: string;
  lastName?: string;
  bio?: string;
  role?: Role;
  photo: string;
}
