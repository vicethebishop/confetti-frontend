import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './components/profile/profile.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatInputModule,
  MatSnackBarModule,
} from '@angular/material';
import { SettingsComponent } from './components/settings/settings.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProfileComponent, SettingsComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatSnackBarModule,
  ],
  exports: [ProfileComponent, SettingsComponent],
})
export class UserFeaturesModule {}
