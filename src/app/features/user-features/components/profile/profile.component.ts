import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { UserModel } from '../../model/user.model';
import { SafeUrl } from '@angular/platform-browser';
import { TokenService } from '../../../../auth/service/token/token.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent implements OnInit {
  @Input() user: UserModel;
  @Input() photo: SafeUrl;
  @Output() photoUpload = new EventEmitter<File>();

  showPhotoUpload: boolean;

  constructor(private readonly tokenService: TokenService) {}

  ngOnInit() {
    this.showPhotoUpload = this.tokenService.userId === this.user.id;
  }

  get fullName() {
    const { firstName, lastName } = this.user;
    return `${firstName} ${lastName}`;
  }

  handleUpload(event: Event) {
    this.photoUpload.emit((event.target as HTMLInputElement).files[0]);
  }
}
