import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { AuthService } from '../../../../auth/service/auth/auth.service';
import {
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  emailForm: FormGroup;
  passwordForm: FormGroup;

  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this._initEmailForm();
    this._initPasswordForm();
  }

  ngOnInit() {}

  private _initEmailForm() {
    this.emailForm = this.fb.group(
      {
        first: ['', [Validators.required, Validators.email]],
        second: ['', [Validators.required, Validators.email]],
      },
      { validators: [this.sameValueValidator('first', 'second')] }
    );
  }

  private _initPasswordForm() {
    this.passwordForm = this.fb.group(
      {
        old: ['', Validators.required],
        first: ['', Validators.required],
        second: ['', Validators.required],
      },
      { validators: [this.sameValueValidator('first', 'second')] }
    );
  }

  changeEmail() {
    if (this.emailForm.invalid) {
      this.emailForm.markAsTouched();
      Object.values(this.emailForm.controls).forEach(control =>
        control.markAsTouched()
      );
      return;
    }
    this.userService
      .changeEmail(this.emailForm.get('first').value)
      .subscribe(() => this.authService.logout());
  }

  changePassword() {
    if (this.passwordForm.invalid) {
      this.passwordForm.markAsTouched();
      Object.values(this.passwordForm.controls).forEach(control =>
        control.markAsTouched()
      );
      return;
    }

    const oldPassword = this.passwordForm.get('old').value;
    const newPassword = this.passwordForm.get('first').value;

    this.userService.changePassword(oldPassword, newPassword).subscribe(
      () => this.authService.logout(),
      error =>
        this.snackBar.open(error.error.message, null, {
          duration: 3000,
        })
    );
  }

  sameValueValidator(firstName: string, secondName: string): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } | null => {
      const firstValue = group.get(firstName).value;
      const secondValue = group.get(secondName).value;
      return firstValue === secondValue
        ? null
        : {
            sameValue: true,
          };
    };
  }
}
