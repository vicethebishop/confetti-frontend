import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { UserModel } from '../model/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const ROUTES = {
  photo: (id: number) => `user/${id}/photo`,
  email: () => `user/email`,
  password: () => `user/password`,
};

export const QUERIES = {
  getById: gql`
    query user($id: Int!) {
      user(id: $id) {
        id
        firstName
        lastName
        bio
      }
    }
  `,
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private readonly httpService: HttpClient,
    private readonly apollo: Apollo
  ) {}

  public getById(id: number): Observable<UserModel> {
    return this.apollo
      .query<{ user: UserModel }>({
        query: QUERIES.getById,
        variables: { id: id },
      })
      .pipe(map(result => result.data.user));
  }

  public uploadPhoto(file: File, userId: number) {
    const form: FormData = new FormData();
    form.append('photo', file);
    return this.httpService.post(ROUTES.photo(userId), form);
  }

  public getPhoto(userId: number) {
    return this.httpService.get(ROUTES.photo(userId), {
      responseType: 'blob',
    });
  }

  public changeEmail(newEmail: string) {
    return this.httpService.put(ROUTES.email(), {
      newEmail: newEmail,
    });
  }

  public changePassword(oldPassword: string, newPassword: string) {
    return this.httpService.put(ROUTES.password(), {
      oldPassword: oldPassword,
      newPassword: newPassword,
    });
  }
}
