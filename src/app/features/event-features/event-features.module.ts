import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventProfileComponent } from './comonents/event-profile/event-profile.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ParticipantPreviewComponent } from './comonents/participant-preview/participant-preview.component';
import { ParticipantPreviewItemComponent } from './comonents/participant-preview-item/participant-preview-item.component';
import { RouterModule } from '@angular/router';
import { EventGridComponent } from './comonents/event-grid/event-grid.component';
import { CreateEventDialogComponent } from './comonents/create-event-dialog/create-event-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfettiFormsModule } from '../../cdk/confetti-forms/confetti-forms.module';
import { EditEventComponent } from './comonents/edit-event/edit-event.component';

@NgModule({
  declarations: [
    EventProfileComponent,
    ParticipantPreviewComponent,
    ParticipantPreviewItemComponent,
    EventGridComponent,
    CreateEventDialogComponent,
    EditEventComponent,
  ],
  exports: [EventProfileComponent, EventGridComponent, EditEventComponent],
  entryComponents: [CreateEventDialogComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule,
    MatTableModule,
    MatDialogModule,
    ReactiveFormsModule,
    ConfettiFormsModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatSortModule,
    FormsModule,
  ],
})
export class EventFeaturesModule {}
