import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { ParticipantModel } from '../../model/participant.model';
import { UserService } from '../../../user-features/service/user.service';
import { BehaviorSubject } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-participant-preview-item',
  templateUrl: './participant-preview-item.component.html',
  styleUrls: ['./participant-preview-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParticipantPreviewItemComponent implements OnInit {
  @Input() participant: ParticipantModel;

  photo$ = new BehaviorSubject<SafeUrl>(null);

  constructor(
    private readonly userService: UserService,
    private readonly domSanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.userService
      .getPhoto(this.participant.user.id)
      .subscribe(photo => this.photo$.next(this._getImageUrl(photo)));
  }

  private _getImageUrl(blob: Blob): SafeUrl {
    return blob.size
      ? this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(blob))
      : '../assets/images/avatar-placeholder.png';
  }
}
