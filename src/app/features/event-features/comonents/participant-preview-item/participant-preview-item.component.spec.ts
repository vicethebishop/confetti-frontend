import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantPreviewItemComponent } from './participant-preview-item.component';

describe('ParticipantPreviewItemComponent', () => {
  let component: ParticipantPreviewItemComponent;
  let fixture: ComponentFixture<ParticipantPreviewItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantPreviewItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantPreviewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
