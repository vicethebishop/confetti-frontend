import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantPreviewComponent } from './participant-preview.component';

describe('ParticipantPreviewComponent', () => {
  let component: ParticipantPreviewComponent;
  let fixture: ComponentFixture<ParticipantPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
