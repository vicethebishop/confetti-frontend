import { Component, OnInit, Input } from '@angular/core';
import { ParticipantModel } from '../../model/participant.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-participant-preview',
  templateUrl: './participant-preview.component.html',
  styleUrls: ['./participant-preview.component.scss'],
})
export class ParticipantPreviewComponent implements OnInit {
  @Input() participants: ParticipantModel[];

  constructor(private readonly router: Router) {}

  ngOnInit() {}

  handleUserRedirect(userId: number) {
    this.router.navigate([`user/${userId}`]);
  }
}
