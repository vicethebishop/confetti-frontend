import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EventModel } from '../../model/event.model';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-event-grid',
  templateUrl: './event-grid.component.html',
  styleUrls: ['./event-grid.component.scss'],
})
export class EventGridComponent implements OnInit {
  @Input() events: EventModel[];

  @Output() new = new EventEmitter();
  @Output() sort = new EventEmitter();

  columns = ['title', 'description', 'startDate'];

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {}

  handleRowClick(id: number) {
    this.router.navigate([`${id}`], { relativeTo: this.activatedRoute });
  }

  handleCreateNew() {
    this.new.emit();
  }

  formatDate(date: Date) {
    return date ? moment(date).format('YYYY-MM-DD') : '';
  }
}
