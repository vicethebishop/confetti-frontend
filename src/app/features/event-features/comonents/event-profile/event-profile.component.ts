import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { EventModel } from '../../model/event.model';
import { MatButton } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-event-profile',
  templateUrl: './event-profile.component.html',
  styleUrls: ['./event-profile.component.scss'],
})
export class EventProfileComponent implements OnInit, OnChanges {
  @Input() event: EventModel;

  @Output() toggleJoin = new EventEmitter<void>();
  @Output() newComment = new EventEmitter<string>();
  @Output() edit = new EventEmitter<void>();

  commentValue = '';

  showNewCommentButtons = false;

  private _joinLabel: string;

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    this._joinLabel = this.event.currentIsParticipant ? 'Joined' : 'Join';
  }

  get joinLabel() {
    return this._joinLabel;
  }

  get startDate() {
    return this.event.startDate
      ? moment(this.event.startDate).format('YYYY-MM-DD')
      : 'Not specified';
  }

  handleJoinHover(type: 'enter' | 'leave', button: MatButton) {
    if (!this.event.currentIsParticipant) {
      return;
    }

    switch (type) {
      case 'enter':
        this._joinLabel = 'Leave';
        button.color = 'accent';
        break;
      case 'leave':
        this._joinLabel = 'Joined';
        button.color = 'primary';
        break;
    }
  }

  handleNewCommentPost() {
    this.newComment.emit(this.commentValue);
    this.commentValue = '';
    this.showNewCommentButtons = false;
  }

  handleCommentCancel() {
    this.commentValue = '';
    this.showNewCommentButtons = false;
  }

  get joinDisabled() {
    return this.event.currentIsParticipant && this.event.currentIsManager;
  }

  getFormattedDate(date: Date) {
    return date ? moment(date).format('YYYY-MM-DD') : '';
  }
}
