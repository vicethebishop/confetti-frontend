import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

const CONTROLS = {
  title: 'title',
  description: 'description',
  startDate: 'startDate',
};

@Component({
  selector: 'app-create-event-dialog',
  templateUrl: './create-event-dialog.component.html',
  styleUrls: ['./create-event-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateEventDialogComponent implements OnInit {
  form: FormGroup;
  CONTROLS = CONTROLS;

  constructor(
    private readonly fb: FormBuilder,
    public dialogRef: MatDialogRef<CreateEventDialogComponent>
  ) {
    this._initForm();
  }

  ngOnInit() {}

  private _initForm() {
    this.form = this.fb.group({
      [CONTROLS.title]: ['', Validators.required],
      [CONTROLS.description]: '',
      [CONTROLS.startDate]: '',
    });
  }

  handleCloseClick() {
    this.dialogRef.close();
  }

  submit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.dialogRef.close(this.form.value);
  }
}
