import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EventModel } from '../../model/event.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';

const CONTROLS = {
  title: 'title',
  description: 'description',
  startDate: 'startDate',
};

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss'],
})
export class EditEventComponent implements OnInit {
  @Input() event: EventModel;

  @Output() save = new EventEmitter<EventModel>();

  form: FormGroup;
  CONTROLS = CONTROLS;

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit() {
    this._initForm();
  }

  private _initForm() {
    const { title, description, startDate } = this.event;

    this.form = this.fb.group({
      [CONTROLS.title]: [title, Validators.required],
      [CONTROLS.description]: description,
      [CONTROLS.startDate]: startDate ? moment(startDate) : '',
    });
  }

  saveEvent() {
    this.save.emit(this.form.value);
  }
}
