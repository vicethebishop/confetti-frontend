import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { EventModel } from '../model/event.model';
import gql from 'graphql-tag';
import { Moment } from 'moment';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  constructor(private readonly apollo: Apollo) {}

  getById(eventId: number) {
    return this.apollo.watchQuery<EventModel>({
      query: gql`
          query {
            event(id: ${eventId}) {
              title
              description
              startDate
              currentIsParticipant
              currentIsManager
              participants {
                manager
                user {
                  id
                  firstName
                  lastName
                }
              }
              posts {
                user {
                  id
                  firstName
                  lastName
                }
                content
                posted
              }
            }
          }
        `,
    });
  }

  getOwnEvents() {
    return this.apollo.watchQuery<EventModel>({
      query: gql`
        query {
          ownEvents(sortBy: "startDate", order: "ASC") {
            id
            title
            description
            startDate
            currentIsParticipant
            participants {
              manager
              user {
                id
                firstName
                lastName
              }
            }
          }
        }
      `,
    });
  }

  create(model: { title: string; description: string; startDate: Moment }) {
    const { title, description, startDate } = model;
    return this.apollo.mutate<EventModel>({
      mutation: gql`
          mutation {
            createEvent(model: { title: "${title}", description: "${description}", startDate: "${startDate.format(
        'YYYY-MM-DD'
      )}" }) {
              id
              title
              description
              currentIsParticipant
              participants {
                manager
                user {
                  id
                  firstName
                  lastName
                }
              }
            }
          }
        `,
    });
  }

  update(model: {
    id: number;
    title: string;
    description: string;
    startDate: Moment;
  }) {
    const { id, title, description, startDate } = model;
    return this.apollo.mutate<EventModel>({
      mutation: gql`
          mutation {
            updateEvent(model: { id: ${id} title: "${title}", description: "${description}", startDate: "${startDate.format(
        'YYYY-MM-DD'
      )}" }) {
              id
              title
              description
              currentIsParticipant
              participants {
                manager
                user {
                  id
                  firstName
                  lastName
                }
              }
            }
          }
        `,
    });
  }

  postComment(eventId: number, content: string) {
    return this.apollo.mutate<EventModel>({
      mutation: gql`
          mutation {
            post(model: { eventId: ${eventId}, content: "${content}" }) {
              title
              description
              currentIsParticipant
              participants {
                manager
                user {
                  id
                  firstName
                  lastName
                }
              }
              posts {
                user {
                  id
                  firstName
                  lastName
                }
                content
                posted
              }
            }
          }
        `,
    });
  }
}
