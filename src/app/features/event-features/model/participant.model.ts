import { UserModel } from '../../user-features/model/user.model';
import { SafeUrl } from '@angular/platform-browser';

export interface ParticipantModel {
  manager: boolean;
  user: UserModel;
  photo: SafeUrl;
}
