import { ParticipantModel } from './participant.model';
import { PostModel } from './post.model';

export interface EventModel {
  id?: number;
  title?: string;
  description?: string;
  startDate?: Date;
  currentIsParticipant: boolean;
  currentIsManager: boolean;
  participants?: ParticipantModel[];
  posts?: PostModel[];
}
