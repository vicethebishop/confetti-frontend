import { UserModel } from '../../user-features/model/user.model';

export interface PostModel {
  id: number;
  content: string;
  posted: Date;
  user: UserModel;
}
