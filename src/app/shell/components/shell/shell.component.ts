import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TokenService } from '../../../auth/service/token/token.service';
import { AuthService } from '../../../auth/service/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShellComponent implements OnInit {
  username: string;

  constructor(
    private readonly tokenService: TokenService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {
    this.username = this.tokenService.username;
  }

  ngOnInit() {
    this.router.navigate(['user/profile']);
  }

  logout() {
    this.authService.logout();
  }
}
