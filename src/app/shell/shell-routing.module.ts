import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ShellComponent } from './components/shell/shell.component';

const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    children: [
      {
        path: 'user',
        loadChildren: () =>
          import('../pages/user-pages/user-pages.module').then(
            m => m.UserPagesModule
          ),
      },
      {
        path: 'events',
        loadChildren: () =>
          import('../pages/event-pages/event-pages.module').then(
            m => m.EventPagesModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShellRoutingModule {}
