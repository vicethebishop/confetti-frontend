import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserModel } from '../../../../features/user-features/model/user.model';
import { TokenService } from '../../../../auth/service/token/token.service';
import { UserService } from '../../../../features/user-features/service/user.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfilePageComponent implements OnInit {
  user$ = new BehaviorSubject<UserModel>(null);
  photo$ = new BehaviorSubject<SafeUrl>(null);

  constructor(
    private readonly tokenService: TokenService,
    private readonly userService: UserService,
    private readonly domSanitizer: DomSanitizer,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    const userId = this.activatedRoute.snapshot.params.id
      ? parseInt(this.activatedRoute.snapshot.params.id, 10)
      : this.tokenService.userId;

    this.userService.getById(userId).subscribe(model => this.user$.next(model));
    this.userService
      .getPhoto(userId)
      .subscribe(photo => this.photo$.next(this._getImageUrl(photo)));
  }

  handleUpload(file: File) {
    this.userService
      .uploadPhoto(file, this.tokenService.userId)
      .pipe(
        switchMap(() => this.userService.getPhoto(this.tokenService.userId))
      )
      .subscribe(photo => this.photo$.next(this._getImageUrl(photo)));
  }

  private _getImageUrl(blob: Blob): SafeUrl {
    return blob.size
      ? this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(blob))
      : '../assets/images/avatar-placeholder.png';
  }
}
