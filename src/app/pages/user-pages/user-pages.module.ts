import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { UserFeaturesModule } from '../../features/user-features/user-features.module';
import { UserRoutingModule } from './user-routing.module';
import { SettingsPageComponent } from './pages/settings-page/settings-page.component';

@NgModule({
  declarations: [ProfilePageComponent, SettingsPageComponent],
  imports: [CommonModule, UserRoutingModule, UserFeaturesModule],
})
export class UserPagesModule {}
