import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventPageComponent } from './components/event-page/event-page.component';
import { EventRoutingModule } from './event-routing.module';
import { EventFeaturesModule } from '../../features/event-features/event-features.module';
import { EventGridPageComponent } from './components/event-grid-page/event-grid-page.component';
import { EditEventPageComponent } from './components/edit-event-page/edit-event-page.component';

@NgModule({
  declarations: [
    EventPageComponent,
    EventGridPageComponent,
    EditEventPageComponent,
  ],
  imports: [CommonModule, EventRoutingModule, EventFeaturesModule],
})
export class EventPagesModule {}
