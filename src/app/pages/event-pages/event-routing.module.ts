import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventPageComponent } from './components/event-page/event-page.component';
import { EventGridPageComponent } from './components/event-grid-page/event-grid-page.component';
import { EditEventPageComponent } from './components/edit-event-page/edit-event-page.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: EventGridPageComponent,
        pathMatch: 'exact',
      },
      {
        path: ':id',
        children: [
          { path: '', pathMatch: 'exact', component: EventPageComponent },
          { path: 'edit', component: EditEventPageComponent },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventRoutingModule {}
