import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { EventModel } from '../../../../features/event-features/model/event.model';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { EventService } from '../../../../features/event-features/service/event.service';

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.scss'],
})
export class EventPageComponent implements OnInit {
  event$ = new BehaviorSubject<EventModel>(null);

  constructor(
    private readonly apollo: Apollo,
    private readonly activatedRoute: ActivatedRoute,
    private readonly eventService: EventService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    const eventId = this.activatedRoute.snapshot.params.id;

    this.apollo
      .watchQuery<EventModel>({
        query: gql`
          query {
            event(id: ${eventId}) {
              title
              description
              startDate
              currentIsParticipant
              currentIsManager
              participants {
                manager
                user {
                  id
                  firstName
                  lastName
                }
              }
              posts {
                user {
                  id
                  firstName
                  lastName
                }
                content
                posted
              }
            }
          }
        `,
      })
      .valueChanges.subscribe((result: any) =>
        this.event$.next(result.data.event)
      );
  }

  toggleJoin() {
    const eventId = this.activatedRoute.snapshot.params.id;

    this.apollo
      .mutate<EventModel>({
        mutation: gql`
          mutation {
            participation(id: ${eventId}) {
              title
              description
              currentIsParticipant
              participants {
                manager
                user {
                  id
                  firstName
                  lastName
                }
              }
            }
          }
        `,
      })
      .subscribe((value: any) => this.event$.next(value.data.participation));
  }

  postComment(content: string) {
    const eventId = this.activatedRoute.snapshot.params.id;
    this.eventService
      .postComment(eventId, content)
      .subscribe((value: any) => this.event$.next(value.data.post));
  }

  handleEditRedirect() {
    this.router.navigate(['edit'], {
      relativeTo: this.activatedRoute,
    });
  }
}
