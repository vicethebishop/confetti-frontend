import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EventModel } from '../../../../features/event-features/model/event.model';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { MatDialog } from '@angular/material';
import { CreateEventDialogComponent } from '../../../../features/event-features/comonents/create-event-dialog/create-event-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Moment } from 'moment';
import { EventService } from '../../../../features/event-features/service/event.service';

@Component({
  selector: 'app-event-grid-page',
  templateUrl: './event-grid-page.component.html',
  styleUrls: ['./event-grid-page.component.scss'],
})
export class EventGridPageComponent implements OnInit {
  events$ = new BehaviorSubject<EventModel[]>([]);

  constructor(
    private readonly apollo: Apollo,
    public dialog: MatDialog,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly eventService: EventService
  ) {}

  ngOnInit() {
    this.eventService
      .getOwnEvents()
      .valueChanges.subscribe((result: any) =>
        this.events$.next(result.data.ownEvents)
      );
  }

  handleCreation() {
    this.dialog
      .open(CreateEventDialogComponent, {
        disableClose: true,
        width: '250px',
      })
      .afterClosed()
      .subscribe(value => {
        if (value) {
          this.create(value);
        }
      });
  }

  private create(model: {
    title: string;
    description: string;
    startDate: Moment;
  }) {
    this.eventService.create(model).subscribe((value: any) =>
      this.router.navigate([`${value.data.createEvent.id}`], {
        relativeTo: this.activatedRoute,
      })
    );
  }
}
