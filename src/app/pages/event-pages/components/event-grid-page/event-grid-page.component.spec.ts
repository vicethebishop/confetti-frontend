import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGridPageComponent } from './event-grid-page.component';

describe('EventGridPageComponent', () => {
  let component: EventGridPageComponent;
  let fixture: ComponentFixture<EventGridPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGridPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGridPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
