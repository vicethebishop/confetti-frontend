import { Component, OnInit } from '@angular/core';
import { EventService } from '../../../../features/event-features/service/event.service';
import { BehaviorSubject } from 'rxjs';
import { EventModel } from '../../../../features/event-features/model/event.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-event-page',
  templateUrl: './edit-event-page.component.html',
  styleUrls: ['./edit-event-page.component.scss'],
})
export class EditEventPageComponent implements OnInit {
  event$ = new BehaviorSubject<EventModel>(null);

  constructor(
    private readonly eventService: EventService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    const eventId = this.activatedRoute.snapshot.params.id;

    this.eventService
      .getById(eventId)
      .valueChanges.subscribe((result: any) =>
        this.event$.next(result.data.event)
      );
  }

  handleSave(event: EventModel) {
    event.id = this.activatedRoute.snapshot.params.id;
    this.eventService.update(event as any).subscribe(value =>
      this.router.navigate([`..`], {
        relativeTo: this.activatedRoute,
      })
    );
  }
}
