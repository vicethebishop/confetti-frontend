import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../../auth/service/token/token.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private readonly tokenService: TokenService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const credentials = this.tokenService.credentials;

    if (credentials && !request.url.includes('auth')) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${credentials.token.access_token}`,
        },
      });
    }
    return next.handle(request);
  }
}
